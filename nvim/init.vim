" Prv's Vimrc Extravaganza


" Stuff that I do not know about
" And is to afraid to touch
version 6.0
if &cp | set nocp | endif
map Q gq
let s:cpo_save=&cpo
set cpo&vim
vmap gx <Plug>NetrwBrowseXVis
nmap gx <Plug>NetrwBrowseX
vnoremap <silent> <Plug>NetrwBrowseXVis :call netrw#BrowseXVis()
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#BrowseX(expand((exists("g:netrw_gx")? g:netrw_gx : '<cfile>')),netrw#CheckIfRemote())
inoremap  u
let &cpo=s:cpo_save
unlet s:cpo_save
set backspace=indent,eol,start
set display=truncate
set fileencodings=ucs-bom,utf-8,default,latin1
set foldcolumn=1
set helplang=en
set history=200
set hlsearch
set incsearch
set ignorecase
set langnoremap
set lazyredraw
set nolangremap
set nomodeline
set nrformats=bin,hex
set number
set ruler
set scrolloff=5
set showcmd
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc
set title
set ttimeout
set ttimeoutlen=100
set wildmenu
" vim: set ft=vim :

filetype plugin indent on
syntax on
"
"=== [ Colour ] ===

set background=dark
colorscheme kuroi
" colorscheme spacegray

"=== [ 4 Width Tab ] ===

set tabstop=4
set softtabstop=0 noexpandtab
set shiftwidth=4

"=== [ Uncode defaults ] ===

setglobal termencoding=utf-8 fileencodings=
scriptencoding utf-8
set encoding=utf-8

autocmd BufNewFile,BufRead * try
autocmd BufNewFile,BufRead * set encoding=utf-8
autocmd BufNewFile,BufRead * endtry

"=== [ Lazy Shift Finger Fix ] ===

command -complete=file -bang -nargs=? W  :w<bang> <args>
command -complete=file -bang -nargs=? Wq :wq<bang> <args>

"=== [ Recursive Path Finding ] ===

set path+=**

"=== [ File Browsing ] ===

let g:netrw_banner=0 " Be gone banner!
let g:netrw_browse_split=4
let g:netrw_altv=1
let g:netrw_liststyle=3
let g:netrw_list_hide=',\(^\|\s\s\)\zs\.\S\+'
let g:netrw_winsize=25

"=== [ Smart Behavior ] ===

set smartcase
set smartindent
set smarttab

"=== [ Right Splits ] ===

set splitright






