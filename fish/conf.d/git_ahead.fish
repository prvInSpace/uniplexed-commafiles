function git_ahead -a ahead behind diverged none
  not git_is_repo; and return

  set -l commit_count (command git rev-list --count --left-right "@{upstream}...HEAD" 2> /dev/null)

  switch "$commit_count"
  case ""
    # no upstream
  case "0"\t"0"
    test -n "$none"; and echo -n "$none"; or echo -n ""
  case "*"\t"0"
    test -n "$behind"; and echo -n "$behind"; or echo -n "-"
  case "0"\t"*"
    test -n "$ahead"; and echo -n "$ahead"; or echo -n "+"
  case "*"
    test -n "$diverged"; and echo -n "$diverged"; or echo -n "±"
  end
end
