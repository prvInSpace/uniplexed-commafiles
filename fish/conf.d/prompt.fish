
function fish_prompt
	echo ''
	set_color red
	echo -n '['
	set_color yellow
	echo -n $USER
	set_color green
	echo -n '@'
	set_color blue
	echo -n (hostname)
	set_color red
	echo -n '] '
	set_color blue
	echo -n (prompt_pwd)
	
	if git_is_repo
		echo -n ' '
		
		if git_is_dirty
			set_color yellow
			echo -n '~'
		else
			set_color green
			echo -n \u2713
		end

		set_color brmagenta
		echo -n '('
		echo -n (git_branch_name)
		echo -n ')'	
		set_color yellow
		git_ahead
	end
	set_color normal
	echo -n ' > '
end


