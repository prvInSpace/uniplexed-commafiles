
function vim
	command nvim $argv
end

function ls_old
	command exa $argv
end

function l
	command ls -la --color $argv
end
alias ls='ls --color'
alias cls='clear; echo -en "\e[3J"'
alias gaa="git add -A"
alias gc="git commit"
alias back="prevd"

alias fishconf="cd ~/.config/fish/conf.d/"

