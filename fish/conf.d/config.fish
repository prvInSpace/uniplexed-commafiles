
set -x PATH /home/prv/.local/bin/ $PATH
set -x EDITOR nvim
set -x HOSTNAME work

set -x GNUPGHOME "$XDG_DATA_HOME"/gnupg

set XDG_DATA_DIRS /var/lib/snapd/desktop/:$XDG_DATA_DIRS
set snap_xdg_path /var/lib/snapd/desktop

fish_vi_key_bindings

function fish_greeting
	echo "Croeso yn ôl $USER!"
end
